<?php

    //constructor
    class Dosis extends CI_Model{
      //funcion constructor
        public function __construct(){
            parent:: __construct();
        }

        //funcion para insetar datos
        public function insertar($datos){
            return $this->db->insert('dosis',$datos);
        }

        public function actualizar($id_dosis,$datos){
          $this->db->where("id_dosis",$id_dosis);
            return $this->db->update("dosis",$datos);
        }

        public function consultarPorId($id_dosis){
          $this->db->where("id_dosis",$id_dosis);
          $this->db->join("vacuna","vacuna.id_va=dosis.fk_id_va");
          $this->db->join("persona","persona.id_persona=dosis.fk_id_persona");
            $dosis=$this->db->get('dosis');
            if ($dosis->num_rows()>0) {
                return $dosis->row();
            } else {
                return false;
            }

        }

        public function consultarTodos(){
          $this->db->join("vacuna","vacuna.id_va=dosis.fk_id_va");
          $this->db->join("persona","persona.id_persona=dosis.fk_id_persona");
            $listadoDosiss=$this->db->get('dosis');
            if ($listadoDosiss->num_rows()>0) {

                return $listadoDosiss;
            } else {
                return false;
            }
        }

        public function eliminar($id_dosis){
        $this->db->where("id_dosis",$id_dosis);
        return $this->db->delete("dosis");
    }
    public function obtenerDosissPorEstado($estado){
      $this->db->where("estado_dosis",$estado);
      $dosiss=$this->db->get("dosis");
      if($dosiss->num_rows()>0){
        return $dosiss;
      }else{
        return false;
      }
    }


  }   //fin llave

 ?>
