<?php

    //constructor
    class Genero extends CI_Model{
      //funcion constructor
        public function __construct(){
            parent:: __construct();
        }

        //funcion para insetar datos
        public function insertar($datos){
            return $this->db->insert('genero',$datos);
        }

        public function actualizar($id_genero,$datos){
          $this->db->where("id_genero",$id_genero);
            return $this->db->update("genero",$datos);
        }

        public function consultarPorId($id_genero){
          $this->db->where("id_genero",$id_genero);
            $genero=$this->db->get('genero');
            if ($genero->num_rows()>0) {

                return $genero->row();
            } else {
                return false;
            }

        }

        public function consultarTodos(){
            $listadoGeneros=$this->db->get('genero');
            if ($listadoGeneros->num_rows()>0){
                return $listadoGeneros;
            }else {
                return false;
            }
        }

        public function eliminar($id_genero){
        $this->db->where("id_genero",$id_genero);
        return $this->db->delete("genero");
    }


  }   //fin llave

 ?>
