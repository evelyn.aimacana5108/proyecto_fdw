<?php

    //constructor
    class Persona extends CI_Model{
      //funcion constructor
        public function __construct(){
            parent:: __construct();
        }

        //funcion para insetar datos
        public function insertar($datos){
            return $this->db->insert('persona',$datos);
        }

        public function actualizar($id_persona,$datos){
          $this->db->where("id_persona",$id_persona);
            return $this->db->update("persona",$datos);
        }

        public function consultarPorId($id_persona){
          $this->db->where("id_persona",$id_persona);
          $this->db->join("genero","genero.id_genero=persona.fk_id_genero");
            $persona=$this->db->get('persona');
            if ($persona->num_rows()>0) {

                return $persona->row();
            } else {
                return false;
            }

        }

        public function consultarTodos(){
          $this->db->join("genero","genero.id_genero=persona.fk_id_genero");
            $listadoPersonas=$this->db->get('persona');
            if ($listadoPersonas->num_rows()>0) {

                return $listadoPersonas;
            } else {

                return false;
            }
        }

        public function eliminar($id_persona){
        $this->db->where("id_persona",$id_persona);
        return $this->db->delete("persona");
    }


  }   //fin llave

 ?>
