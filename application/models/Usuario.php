<?php
class Usuario extends CI_Model {
  public function __construct(){
    parent::__construct();
  }

//consulta login usuario FUNCION
public function buscarUsuarioPorEmailPassword($email_usuario, $password_usuario){

    $this->db->where("email_usuario",$email_usuario);
    $this->db->where("password_usuario",$password_usuario);
    $usuarioEncontrado=$this->db->get("usuario");
    if($usuarioEncontrado->num_rows()>0){
        return $usuarioEncontrado->row();

    }else{//cuando las credenciales son incorrectas
        return false;
    }

}

public function insertarUsuario($data){
  return $this->db->insert('usuario',$data);
}

public function obtenerTodos(){
$listado=$this->db->get("usuario");
if ($listado->num_rows()>0) {
  return $listado;
} else {
  return false;

    }
  }


  public function eliminar($id_usuario){
    $this->db->where("id_usuario",$id_usuario);
  return  $this->db->delete("usuario");
  }


  public function obtenerPorId($id_usuario){
                  $this->db->where("id_usuario",$id_usuario);
                  $usuario=$this->db->get("usuario");
                  if($usuario->num_rows()>0){
                    return $usuario->row();
                  }else{//cuando las credenciales estan incorrectas
                    return false;
                  }
                    }

  public function actualizar($data, $id_usuario){
        $this->db->where("id_usuario",$id_usuario);
        return $this->db->update("usuario",$data);
    }
    public function obtenerPorEmail($email_usuario){
      $this->db->where("email_usuario",$email_usuario);
      $usuario=$this->db->get("usuario");
      if ($usuario->num_rows()>0) {
        return $usuario->row();
      }else{
        return false;
      }

    }
}//cierre de la clase usuario

  ?>
