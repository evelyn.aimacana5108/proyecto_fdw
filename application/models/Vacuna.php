<?php
class Vacuna extends CI_Model {
  public function __construct(){
    parent::__construct();
  }

public function insertarVacuna($data){
  return $this->db->insert('vacuna',$data);
}

public function obtenerTodos(){
$listado=$this->db->get("vacuna");
if ($listado->num_rows()>0) {
  return $listado;
} else {
  return false;

    }
  }
  public function eliminar($id_va){
    $this->db->where("id_va",$id_va);
  return  $this->db->delete("vacuna");
  }


  public function obtenerPorId($id_va){
                  $this->db->where("id_va",$id_va);
                  $vacuna=$this->db->get("vacuna");
                  if($vacuna->num_rows()>0){
                    return $vacuna->row();
                  }else{//cuando las credenciales estan incorrectas
                    return false;
                  }
                    }
                    public function obtenerPorNombre($tipo_va){
                      $this->db->where("tipo_va",$tipo_va);
                      $vacuna=$this->db->get("vacuna");
                      if ($vacuna->num_rows()>0) {
                        return $vacuna->row();
                      }else{
                        return false;
                      }

                    }

  public function actualizar($data, $id_va){
        $this->db->where("id_va",$id_va);
        return $this->db->update("vacuna",$data);
    }

}//cierre de la clase usuario

  ?>
