<br>
<center>
  <b><i><h2>Listado de Personas</h2></i></i></b>
</center>
<hr>
<br>
<center>
  <div class=" col-12 d-grid gap-2 col-6 mx-auto text-uppercase">
  <button  class="btn btn-secondary" type="button"><b><a href="<?php echo site_url(); ?>/personas/nuevo" ><i class="fa fa-plus-circle"></i>Agregar Personas</a></b></button>
</div>
<hr>
  <br>
  <br>
  <br>
  <br>
</center>
<!-- <div class="col-12">

</div> -->

<?php if ($listadoPersonas): ?>
  <table class="table table-bordered table-striped table-hover" id="tbl-personas">
    <thead class="table-dark">
    <tr>
      <th class="text-center">ID</th>
      <th class="text-center">FOTO</th>
      <th class="text-center">CEDULA</th>
      <th class="text-center">NOMBRE</th>
      <th class="text-center">GENERO</th>
      <th class="text-center">OPCIONES</th>
    </tr>
    </thead>
    <tbody>
      <?php foreach ($listadoPersonas->result() as $filaTemporal): ?>
        <tr>
          <td class="text-center"><?php echo $filaTemporal->id_persona;?></td>
          <td class="text-center">
                               <?php if ($filaTemporal->foto_persona!=""): ?>
                                 <img
                                 src="<?php echo base_url(); ?>/uploads/personas/<?php echo $filaTemporal->foto_persona; ?>"
                                 height="80px"
                                 width="100px"
                                 alt="">
                               <?php else: ?>
                                 N/A
                               <?php endif; ?>
                             </td>
          <td class="text-center"><?php echo $filaTemporal->cedula_persona;?></td>
          <td class="text-center"><?php echo $filaTemporal->nombre_persona;?></td>
          <th><?php echo $filaTemporal->nombre_genero; ?></th>


<td class="text-center">
  <a href="<?php echo site_url(); ?>/Personas/editar/<?php echo $filaTemporal->id_persona;?>"class="btn btn-warning"><i class="fa fa-pen"></i></a>
    <a onclick="confirmarEliminacion('<?php echo $filaTemporal->id_persona; ?>')" href="javascript:void(0)" class="btn btn-danger"><i class="fa fa-trash"></i></a>
</td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
<?php else: ?>
  <div class="alert alert-danger">
    <h3>No se encontraron personas resgistrados</h3>
  </div>
<?php endif; ?>

<script type="text/javascript">
    function confirmarEliminacion(id_persona){
          iziToast.question({
              timeout: 20000,
              close: false,
              overlay: true,
              displayMode: 'once',
              id: 'question',
              zindex: 999,
              title: 'CONFIRMACIÓN',
              message: '¿Esta seguro de eliminar el persona de forma pernante?',
              position: 'center',
              buttons: [
                  ['<button><b>SI</b></button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                      window.location.href=
                      "<?php echo site_url(); ?>/personas/procesarEliminacion/"+id_persona;

                  }, true],
                  ['<button>NO</button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                  }],
              ]
          });
    }
</script>
<script type="text/javascript">
$(document).ready(function() {
  $('#tbl-personas').DataTable( {
    dom: 'Blfrtip',
    buttons: [
        'copyHtml5',
        'excelHtml5',
        'csvHtml5',
        'pdfHtml5'
    ],
      "order": [[ 3, "desc" ]],
    language: {
      "decimal":        "",
  "emptyTable":     "No hay datos",
  "info":           "Mostrando START a END de TOTAL registros",
  "infoEmpty":      "Mostrando 0 a 0 de 0 registros",
  "infoFiltered":   "(Filtro de MAX total registros)",
  "infoPostFix":    "",
  "thousands":      ",",
  "lengthMenu":     "Mostrar MENU registros",
  "loadingRecords": "Cargando...",
  "processing":     "Procesando...",
  "search":         "Buscar:",
  "zeroRecords":    "No se encontraron coincidencias",
  "paginate": {
      "first":      "Primero",
      "last":       "Ultimo",
      "next":       "Próximo",
      "previous":   "Anterior"
  },
  "aria": {
      "sortAscending":  ": Activar orden de columna ascendente",
      "sortDescending": ": Activar orden de columna desendente"
  }
    }
  } );
} );

</script>
