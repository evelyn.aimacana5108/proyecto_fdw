<br>
<form  action="<?php echo site_url(); ?>/personas/procesarActualizacion" method="post" enctype="multipart/form-data">
<input type="hidden" name="id_persona"  id="id_persona" value="<?php echo $persona->id_persona; ?>">
<div class="col-12 grid-margin">
  <div class="card">
    <div class="card-body">
      <h4 class="card-title">INGRESAR DATOS</h4>
      <form class="form-sample">
        <p class="card-description"> Ingrese todos los campos vacios </p>
        <br>
<!-- <div class="row"> -->
<table class="table table-success table-striped">
  <div class="col-md-12">

    <div class="col-md-4">
      <div class="form-group row">
        <label class="col-sm-3 col-form-label">Identificacion:</label>
        <div class="col-sm-9">
           <input type="text" class="form-control" value="<?php echo $persona->cedula_persona; ?>" type="text" name="cedula_persona" id="cedula_persona" value="" placeholder="Por favor ingrese la identificacion">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-sm-3 col-form-label">Nombre:</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" value="<?php echo $persona->nombre_persona; ?>" type="text" name="nombre_persona" id= "nombre_persona"value="" placeholder="Ingrese su nombre">
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group row">
        <label class="col-sm-3 col-form-label">Genero</label>
        <div class="col-sm-9">
          <select class="form-control" name="fk_id_genero" id="fk_id_genero" required>
            <option value="">----Seleccione género-----</option>
            <?php if ($listadoGeneros): ?>
              <?php foreach ($listadoGeneros->result() as $generoTemporal): ?>
                <option value="<?php echo $generoTemporal->id_genero; ?>"><?php echo $generoTemporal->nombre_genero; ?></option>
              <?php endforeach; ?>
            <?php endif; ?>

          </select>
        </div>
      </div>
    </div>
  <br>
  <label for="">FOTOGRAFIA</label>
  <input type="file" name="foto_persona"
  accept="image/*"
  id="foto_persona" value="">
  </div>
    <br>
   <div class="row" align="center">
     <div class="col-md-12">
       <div class="form-group row">
         <button type="submit" class="btn btn-dark btn-lg" align="center"><i class="fa fa-save"></i>&nbsp;ACTUALIZAR</button>
         &nbsp;&nbsp;&nbsp;
           <a href="<?php echo site_url('personas/index'); ?>"class="btn btn-danger btn-lg" align="center"><i class="fa fa-times"></i>&nbsp;CANCELAR</a>
       </div>
     </div>
   </div>
   <input type="range" name="" value="">
</form>
</div>
</div>
</div>
</form>
<script type="text/javascript">
$("#fk_id_genero").val("<?php  echo $persona->fk_id_genero; ?>");
</script>
<script type="text/javascript">
  $("#foto_persona").fileinput({
    allowedFileExtension:["jpeg","jpg","png"],
    dropZoneEnabled:true,
    language:"es"
  });
</script>
