<br>
<center>
  <ul class="nav nav-tabs">
    <li class="nav-item">
      <a class="nav-link active" aria-current="page" href="<?php echo site_url('personas/index'); ?>">LISTADO DE PERSONAS</a>
    </li>
  </ul>
</center>
<br>
<form  action="<?php echo site_url(); ?>/personas/guardarPersona" method="post" id="frm_nuevo_persona"  enctype="multipart/form-data">

<br>
<br>
<!-- <div class="row"> -->
<table class="table table-success table-striped">
  <div class="col-12 grid-margin">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">INGRESAR DATOS DE PERSONAS</h4>
        <form class="form-sample">
          <p class="card-description"> Ingrese todos los campos vacios </p>
          <br>
    <!-- inicio -->
    <div class="row">
  <div class="col-md-4">
    <div class="form-group row">
      <label class="col-sm-3 col-form-label">Cedula: </label>
      <div class="col-sm-9">
        <input type="number"  name="cedula_persona" id="cedula_persona" class="form-control" />
      </div>
    </div>
  </div>
  <div class="col-md-4">
    <div class="form-group row">
      <label class="col-sm-3 col-form-label">Nombre: </label>
      <div class="col-sm-9">
        <input type="text"  name="nombre_persona" id= "nombre_persona" class="form-control" placeholder="" />
      </div>
    </div>
  </div>
  <div class="col-md-4">
    <div class="form-group row">
      <label class="col-sm-3 col-form-label">Genero</label>
      <div class="col-sm-9">
        <select class="form-control" name="fk_id_genero" id="fk_id_genero" required>
          <option value="">----Seleccione género-----</option>
          <?php if ($listadoGeneros): ?>
            <?php foreach ($listadoGeneros->result() as $generoTemporal): ?>
              <option value="<?php echo $generoTemporal->id_genero; ?>"><?php echo $generoTemporal->nombre_genero; ?></option>
            <?php endforeach; ?>
          <?php endif; ?>

        </select>
      </div>
    </div>
  </div>
</div>
    <!-- fin -->
  <br><br>
          <label for="">FOTOGRAFIA</label>
          <input type="file" name="foto_persona"
          accept="image/*"
          id="foto_persona" value="">
          <br>
          <br>
    <div class="row">
    <div class="col-md-12 mt-4 mb-4 text-center">
      <button type="submit" class="btn btn-dark btn-lg" align="center"><i class="fa fa-save"></i>Guardar</b></button>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <a href="<?php echo site_url('personas/index'); ?>"class="btn btn-danger btn-lg" align="center"><i class="fa fa-times"></i>&nbsp;CANCELAR</a>
      </a>
    </div>
  </div>
  <input type="range" name="" value="">
</form>
</div>
</div>
</div>
</form>
<script type="text/javascript">
  $('#frm_nuevo_persona').validate({
      rules:{
        fk_id_genero:{
          required:true,

        },
        cedula_persona:{
          required:true,
          minlength:10,
          maxlength:10,
          digits:true,
        },
        nombre_persona:{
          letras:true,
          required:true,



        }
      },
      messages:{
        fk_id_genero:{
          required:"Por favor ingrese el genero",

        },
        cedula_persona:{
          required:"Por favor ngrese la cedula",
          minlength:"La cedula debe tener 10 dijitos",
          maxlength:"la cedula debe tener 10 numeros",
          digits:"Solo acepta numeros"
        },
        nombre_persona:{
          letras:"Nombre incorrecto",
          required:"Por favo ingrese el Nombre"



        }
      }

  });
</script>
<!-- </script> -->
<script type="text/javascript">
  $("#foto_persona").fileinput({
    allowedFileExtension:["jpeg","jpg","png"],
    dropZoneEnabled:true,
    language:"es"
  });
</script>
<!-- <script type="text/javascript">
$('#foto_persona').fileinput({
       theme: 'fa5',
       language: 'es',
       uploadUrl: 'https://www.tooltyp.com/8-beneficios-de-usar-imagenes-en-nuestros-sitios-web/',
       allowedFileExtensions: ['jpg', 'png', 'jpeg']
   });

</script> -->
