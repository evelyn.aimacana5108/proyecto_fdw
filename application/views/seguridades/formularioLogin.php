<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="<?php echo base_url(); ?>/assets/vendor/bootstrap/css/bootstrap.min.css"
crossorigin="anonymous">
<script src="<?php echo base_url(); ?>/assets/vendor/bootstrap/js/bootstrap.min.js"></script>
<link href="<?php echo base_url(); ?>/assets/vendor/fontawesome/css/all.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>/assets/assets/css/style.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-login-form/1.0.0/bootstrap-login-form.min.css" integrity="sha512-Dzi0zz9zCe2olZNhq+wnzGjO5ILOv8f/yD6j8srW+XGnnv9dUN04eEoIdVHxQqiy8uBn21niIWQpiCzYJEH3yg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<!-- Latest compiled JavaScript -->


<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.min.js" integrity="sha384-+sLIOodYLS7CIrQpBjl+C7nPvqq+FbNUBDunl/OZv93DB7Ln/533i8e/mZXLi/P+" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>

<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.4.0/js/iziToast.min.js" integrity="sha512-Zq9o+E00xhhR/7vJ49mxFNJ0KQw1E1TMWkPTxrWcnpfEFDEXgUiwJHIKit93EW/XxE31HSI5GEOW06G6BF1AtA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.4.0/css/iziToast.css" integrity="sha512-DIW4FkYTOxjCqRt7oS9BFO+nVOwDL4bzukDyDtMO7crjUZhwpyrWBFroq+IqRe6VnJkTpRAS6nhDvf0w+wHmxg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<!-- importacion de Jquery validation -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.4/jquery.validate.min.js" integrity="sha512-FOhq9HThdn7ltbK8abmGn60A/EMtEzIzv1rvuh+DqzJtSGq8BRdEN0U+j0iKEIffiw/yEtVuladk6rsG4X6Uqg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.4/additional-methods.min.js" integrity="sha512-XJiEiB5jruAcBaVcXyaXtApKjtNie4aCBZ5nnFDIEFrhGIAvitoqQD6xd9ayp5mLODaCeaXfqQMeVs1ZfhKjRQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.4/localization/messages_es_AR.min.js" integrity="sha512-HHnzo0ssMRoNapdoTaORwzLpemBFMsg7GA8fr0d9xS1rEXKHazYMTUAUka2abGFCfsdXgZPVVyv3LCkXP1Fhsg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css">
<script type="text/javascript" src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>


<script type="text/javascript">
jQuery.validator.addMethod("letras", function(value, element) {
   //return this.optional(element) || /^[a-z]+$/i.test(value);
   return this.optional(element) || /^[A-Za-zÁÉÍÑÓÚáé íñó]*$/.test(value);

 }, "Este campo solo acepta letras");
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.5.0/js/fileinput.min.js" integrity="sha512-C9i+UD9eIMt4Ufev7lkMzz1r7OV8hbAoklKepJW0X6nwu8+ZNV9lXceWAx7pU1RmksTb1VmaLDaopCsJFWSsKQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.5.0/css/fileinput.min.css" integrity="sha512-XHMymTWTeqMm/7VZghZ2qYTdoJyQxdsauxI4dTaBLJa8d1yKC/wxUXh6lB41Mqj88cPKdr1cn10SCemyLcK76A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.5.0/js/locales/es.min.js" integrity="sha512-q2lXTQuccVsDwaOpJNHbGDL2c5DEK706u1MCjKuGAG4zz+q1Sja3l2RuymU3ySE6RfmTYZ/V4wY5Ol71sRvvWA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
</head>
<body>
  <div class="d-flex flex-column min-vh-100 justify-content-center align-items-center"
  id="template-bg-3">

  <div class="card mb-5 p-5 bg-dark bg-gradient text-white col-md-4">
  <div class="card-header text-center">
  <h3><i class="fas fa-user mt-2"></i>Iniciar sesión </h3>
  </div>
  <div class="card-body mt-3">
    <form class="pt-3" action="<?php echo site_url(); ?>/seguridades/validarAcceso" method="post">
                    <div class="form-group">
                        <label for="">EMAIL:</label>
                      <input type="email" class="form-control form-control-lg" name="email_usuario" id="email_usuario" placeholder="Email">
                    </div>
                    <br>
                    <div class="form-group">
                      <label for="">PASSWORD</label>
                      <input type="password" class="form-control form-control-lg"name="password_usuario" id="password_usuario" placeholder="Password">
                    </div>
                    <div class="mt-3">
                      <button type="submit" class="btn btn-primary btn-info btn-lg font-weight-medium auth-form-btn"> Ingresar </button>
</form>
                    </div>
                    <br>
                    <hr>
                    <button type="button" class="btn btn-info btn-lg font-weight-medium auth-form-btn" data-toggle="modal" data-target="#modalNuevoUsuario"><i class="fa fa-plus-circle"></i>Agregar Usuario</i> </button>
                    <button type="button" class="btn btn-warning btn-lg font-weight-medium auth-form-btn" data-toggle="modal" data-target="#modalRecuperarContraseña"><i class="fa fa-plus-circle"></i>RECUPERAR</i> </button>


  <?php if(!empty($loginResult)){?>
  <div class="text-danger"><?php echo $loginResult;?></div>
  <?php }?>
  </div>
  <div class="card-footer p-3">
  <div class="d-flex justify-content-center">
  </div>
  </div>
  </div>
                 <div id="modalNuevoUsuario" style="z-index:9999 !important;" class="modal fade" role="dialog">
                <div class="modal-dialog modal-lg">

                  <!-- Modal content-->
                  <div class="modal-content">
                      <div class="modal-header">
                        <h4 class="modal-title"><i class="fa fa-users"></i> Nuevo Usuario</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                      </div>
                      <div class="modal-body">
                        <form class=""
                        action="<?php echo site_url('seguridades/insertarUsuario'); ?>"
                        method="post"
                        id="frm_nuevo_usuario">
                        <label for="">APELLIDO:</label>
                        <br>
                        <input type="text" name="apellido_usuario"
                        id="apellido_usuario" class="form-control"> <br>

                        <label for="">NOMBRE:</label>
                        <br>
                        <input type="text" name="nombre_usuario"
                        id="nombre_usuario" class="form-control"> <br>

                        <label for="">EMAIL:</label>
                        <br>
                        <input type="text" name="email_usuario"
                        id="email_usuario" class="form-control"> <br>

                        <label for="">CONTRASEÑA:</label>
                        <br>
                        <input type="password" name="password_usuario"
                        id="password_usuario" class="form-control"> <br>

                        <label for="">CONFIRME LA CONTRASEÑA:</label>
                        <br>
                        <input type="password" name="password_confirmada"
                        id="password_confirmada" class="form-control"> <br>

                        <label for="">PERFIL:</label>
                        <br>
                        <select class="form-control" name="perfil_usuario"
                        id="perfil_usuario">
                         <option value="">Seleccione una opción</option>
                         <!-- <option value="ADMINISTRADOR">ADMINISTRADOR</option> -->
                         <option value="VENDEDOR">VENDEDOR</option>
                        </select>
                        <br>
                        <button type="submit" name="button"
                        class="btn btn-success">
                          <i class="fa fa-save"></i> Guardar
                        </button>

                        </form>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                      </div>
                    </div>

                  </div>
                </div>
  </div>
  <div id="modalRecuperarContraseña" style="z-index:9999 !important;" class="modal fade" role="dialog">
 <div class="modal-dialog modal-lg">

   <!-- Modal content-->
   <div class="modal-content">
       <div class="modal-header">
         <h4 class="modal-title"><i class="fa fa-users"></i> Recuperar Contraseña</h4>
         <button type="button" class="close" data-dismiss="modal">&times;</button>
       </div>
       <div class="modal-body">
         <form class=""
         action="<?php echo site_url('seguridades/recuperarPassword'); ?>"
         method="post"
         id="frm_recuperar_contraseña">

         <label for="">EMAIL:</label>
         <br>
         <input type="text" name="email"
         id="email" class="form-control"> <br>
         <br>
         <button type="submit" name="button"
         class="btn btn-success">
           <i class="fa fa-save"></i> Recuperar </button>

         </form>
       </div>
       <div class="modal-footer">
         <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
       </div>
     </div>

   </div>
 </div>

</body>
<script type="text/javascript">
$("#frm_nuevo_usuario").validate({
  rules: {
    apellido_usuario: {
      required: true,
    },
    nombre_usuario:{
      letras:true,
      required:true,
    },
    email_usuario:{
      required:true,
      email:true,

    },
    password_usuario:{
      required:true,
      minlength:6,
      maxlength:10,
      digits:true,

    },
    password_confirmada:{
      required:true,
      equalto:"#password_usu",
    },
    perfil_usuario:{
      required:true,
    },

  },
  messages: {
    apellido_usuario:{
      required: "Por favor ingrese el apellido",
    },
    nombre_usuario:{
      letras:"Nombre incorrecto",
      required:"Por favo ingrese el Nombre"
    },
    email_usuario:{
      required:"Solo acepta correos electronicos validos",
      email:"Correro incorrecto"

    },
    password_usuario:{
      required:"Por favo ingrese contraseña",
      minlength:"Telefono debe tener 6 dijitos",
      maxlength:"Telefono debe tener 10 dijitos",
      digits:"Solo acepta numeros"

    },
    password_confirmada_usuario:{
      required:"Por favo ingrese contraseña",
      minlength:"Telefono debe tener 6 dijitos",
      maxlength:"Telefono debe tener 10 dijitos",
      digits:"Solo acepta numeros",
      equalto:"ingrese la misma contraseña"
    },
    perfil_usuario:{
      required:"Ingrese la direccion",
    }

  }
});
  </script>

  <script>
  submitHandler:function(form){
    $.ajax({
    url:$(form).prop("action"),
    type: "post",
    data:$(form).serialize(),
    success:function(data){
      cargarlistadoUsuarios(),
      $("#modalNuevoUsuario").modal("hide");
      $('body').removeClass('modal-open');
      $('.modal-backdrop').remove();
      var objetoJson=JSON.parse(data);
        if(objetoJson.respuesta=="ok" || objetoJson.respuesta=="OK"){
            alert("Insercion Exitosa");

              }else{
                  alert("Error");
                }
      alert(data);
    }
    });
  }
});
</script>
</html>
 <br>
 <!-- <form class="" action="<?php echo site_url("seguridades/recuperarPassword"); ?>" method="post" align="center">
   <h3><i class="fa fa-times">RECUPERAR CONTRASEÑA</i></h3>
   <label for="">Ingrese su Email</label>
   <input type="email" name="email" value="">
   <button type="submit" name="button">RECUPERAR AHORA</button>
 </form> -->
