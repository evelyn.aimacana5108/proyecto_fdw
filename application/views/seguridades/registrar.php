<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<br>
<h1 class="text-center">GESTION DE USUARIOS</h1>
<br>
<center>




 <!-- Modal -->
 <div id="modalNuevoUsuario"
 style="z-index:9999 !important;"
  class="modal fade" role="dialog">
   <div class="modal-dialog modal-lg">

     <!-- Modal content-->
     <div class="modal-content">
       <div class="modal-header">
         <h4 class="modal-title"><i class="fa fa-users"></i> NUEVO USUARIO</h4>
         <button type="button" class="close" data-dismiss="modal">&times;</button>
       </div>
       <div class="modal-body">
         <form class=""
         action="<?php echo site_url('seguridades/insertarUsuario'); ?>"
         method="post"
         id="frm_nuevo_usuario">
         <label for="">APELLIDO:</label>
         <br>
         <input type="text" name="apellido_usuario"
         id="apellido_usuario" class="form-control"> <br>

         <label for="">NOMBRE:</label>
         <br>
         <input type="text" name="nombre_usuario"
         id="nombre_usuario" class="form-control"> <br>

         <label for="">EMAIL:</label>
         <br>
         <input type="text" name="email_usuario"
         id="email_usuario" class="form-control"> <br>

         <label for="">CONTRASEÑA:</label>
         <br>
         <input type="password" name="password_usuario"
         id="password_usuario" class="form-control"> <br>

         <label for="">CONFIRME LA CONTRASEÑA:</label>
         <br>
         <input type="password" name="password_confirmada"
         id="password_confirmada" class="form-control"> <br>

         <label for="">PERFIL:</label>
         <br>
         <select class="form-control" name="perfil_usuario"
         id="perfil_usuario">
          <option value="">Seleccione una opción</option>
          <option value="ADMINISTRADOR">ADMINISTRADOR</option>
          <option value="VENDEDOR">VENDEDOR</option>
         </select>
         <br>
         <button type="submit" name="button"
         class="btn btn-success">
           <i class="fa fa-save"></i> GUARDAR
         </button>

         </form>
       </div>
       <div class="modal-footer">
         <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
       </div>
     </div>

   </div>
 </div>


<script type="text/javascript">

    $("#frm_nuevo_usuario").validate({
      rules:{
        apellido_usuario:{
          required:true,
        },
        nombre_usuario:{
          required:true,
        },
        email_usuario:{
          required:true,
        },
        password_usuario:{
          required:true
        },
        password_confirmada:{
          required:true,
          equalTo:"#password_usuario"
        },
        perfil_usuario:{
          required:true,
        }
      },
      messages:{
        apellido_usuario:{
          required:"Por favor ingrese el apellido",
        },
        nombre_usuario:{
          required:"Por favor ingrese el nombre",
        },
        email_usuario:{
          required:"Por favor ingrese el email",
        },
        password_usuario:{
          required:"Por favor ingrese la contraseña",
        },
        password_confirmada:{
          required:"Por favor ingrese la contraseña",
        },
      },
      submitHandler:function(form){//funcion para peticiones AJAX
          $.ajax({
            url:$(form).prop("action"),
            type:"post",
            data:$(form).serialize(),
            success:function(data){
                cargarListadoUsuarios();
                $("#modalNuevoUsuario").modal("hide");
                var objetoJson=JSON.parse(data);
                  if(objetoJson.respuesta=="ok"||objetoJson.respuesta=="OK"){
                    iziToast.success({
                         title: 'CONFIRMACIÓN',
                         message: 'Usuario Insertado con exito',
                         position: 'topRight',
                       });
                  }else{
                    iziToast.error({
                         title: 'ERROR',
                         message: 'ERROR',
                         position: 'topRight',
                       });
                  }
                //backdrop
                alert(data);
            }
        });
      }
    });
</script>
