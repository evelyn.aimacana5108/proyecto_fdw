<script type="text/javascript" src="//cdn.datatables.net/1.11.4/js/jquery.dataTables.min.js"> </script>
      <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.2/js/dataTables.buttons.min.js"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
      <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.html5.min.js"></script>
<!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="d-sm-flex justify-content-center justify-content-sm-between">
            <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2021.  Premium <a href="https://www.bootstrapdash.com/" target="_blank">Bootstrap admin template</a> from BootstrapDash. All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="ti-heart text-danger ml-1"></i></span>
          </div>
          <div class="d-sm-flex justify-content-center justify-content-sm-between">
            <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Distributed by <a href="https://www.themewagon.com/" target="_blank">Themewagon</a></span>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

  <!-- plugins:js -->
  <script src="<?php echo base_url(); ?>/assets/vendors/js/vendor.bundle.base.js"></script>
  <!-- endinject -->
  <!-- Plugin js for this page -->
  <script src="<?php echo base_url(); ?>/assets/vendors/chart.js/Chart.min.js"></script>
  <script src="<?php echo base_url(); ?>/assets/vendors/datatables.net/jquery.dataTables.js"></script>
  <script src="<?php echo base_url(); ?>/assets/vendors/datatables.net-bs4/dataTables.bootstrap4.js"></script>
  <script src="<?php echo base_url(); ?>/assets/js/dataTables.select.min.js"></script>

  <!-- End plugin js for this page -->
  <!-- inject:js -->
  <script src="<?php echo base_url(); ?>/assets/js/off-canvas.js"></script>
  <script src="<?php echo base_url(); ?>/assets/js/hoverable-collapse.js"></script>
  <script src="<?php echo base_url(); ?>/assets/js/template.js"></script>
  <script src="<?php echo base_url(); ?>/assets/js/settings.js"></script>
  <script src="<?php echo base_url(); ?>/assets/js/todolist.js"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="<?php echo base_url(); ?>/assets/js/dashboard.js"></script>
  <script src="<?php echo base_url(); ?>/assets/js/Chart.roundedBarCharts.js"></script>
  <!-- End custom js for this page-->
<!-- inicio -->
<?php if ($this->session->flashdata('confirmacion')): ?>
  <script type="text/javascript">
  iziToast.success({
      title: 'INSERCIÓN',
      message: '<?php echo $this->session->flashdata('confirmacion');?>',
      position: 'topRight'
  });
  </script>

<?php endif; ?>
<!-- actualizacion -->
<?php if ($this->session->flashdata('actualizacion')): ?>
  <script type="text/javascript">
  iziToast.success({
      title: 'ACTUALIZACIÓN',
      message: '<?php echo $this->session->flashdata('actualizacion');?>',
      position: 'topRight'
  });
  </script>

<?php endif; ?>
<!-- eliminacion -->
<?php if ($this->session->flashdata('eliminacion')): ?>
  <script type="text/javascript">
  iziToast.success({
      title: 'ELIMINACIÓN',
      message: '<?php echo $this->session->flashdata('eliminacion');?>',
      position: 'topRight'
  });
  </script>

<?php endif; ?>
<?php if ($this->session->flashdata('error')): ?>
  <script type="text/javascript">
  iziToast.error({
      title: 'ERROR',
      message: '<?php echo $this->session->flashdata('error');?>',
      position: 'topRight'
  });
  </script>

<?php endif; ?>

<?php if ($this->session->flashdata('bienvenida')): ?>
  <script type="text/javascript">
  iziToast.info({
      title: 'CONFIRMACIÓN',
      message: '<?php echo $this->session->flashdata('bienvenida');?>',
      position: 'topRight'
  });
  </script>
<?php endif; ?>
<?php if ($this->session->flashdata('salir')): ?>
  <script type="text/javascript">
  iziToast.success({
      title: 'Salir',
      message: '<?php echo $this->session->flashdata('salir');?>',
      position: 'topRight'
  });
  </script>

<?php endif; ?>

<script type="text/javascript">
    function salirSistema(salir){
          iziToast.info({
              timeout: 20000,
              close: false,
              overlay: true,
              displayMode: 'once',
              id: 'question',
              zindex: 999,
              title: 'Hey',
              message: 'Esta seguro que desea salir del Sistema?',
              position: 'topRight',
              buttons: [
                  ['<button><b>SI</b></button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                      window.location.href="<?php echo site_url('Accesos/logout')?>";

                  }, true],
                  ['<button>NO</button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                  }],
              ]
          });
    }
</script>

<!-- el archivo de css esta como nombre formulario.css -->


<script type="text/javascript" src="//cdn.datatables.net/1.11.4/js/jquery.dataTables.min.js">
      </script>
      <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.2/js/dataTables.buttons.min.js"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
      <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.html5.min.js"></script>
      <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA8eaHt9Dh5H57Zh0xVTqxVdBFCvFMqFjQ&callback=initMap"></script>


</body>

</html>
