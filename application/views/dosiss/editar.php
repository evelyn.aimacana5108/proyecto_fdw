<br>
<form  action="<?php echo site_url(); ?>/dosiss/procesarActualizacion" method="post" enctype="multipart/form-data">
<!-- <input type="hidden" name="id_dosis"  id="id_dosis" value="<?php echo $cliente->id_dosis; ?>"> -->
<div class="col-12 grid-margin">
<div class="card">
  <div class="card-body">
    <h4 class="card-title">INGRESAR DATOS</h4>
    <form class="form-sample">
<p class="card-description"> Ingrese todos los campos vacios </p>
<br>
<div class="row">
<div class="col-md-6">
  <div class="form-group row">
    <label class="col-sm-3 col-form-label">Vacuna:</label>
    <div class="col-sm-9">
      <!-- required para campos obligatorios -->
      <select class="form-control" name="fk_id_va" id="fk_id_va" required>
        <option value="">----Seleccioneun vacuna-----</option>
        <?php if ($listadoVacunas): ?>
          <?php foreach ($listadoVacunas->result() as $vacunaTemporal): ?>
            <option value="<?php echo $vacunaTemporal->id_va; ?>"><?php echo $vacunaTemporal->tipo_va; ?></option>
          <?php endforeach; ?>
        <?php endif; ?>

      </select>
    </div>
  </div>
</div>
<div class="col-md-6">
  <div class="form-group row">
    <label class="col-sm-3 col-form-label">Persona </label>
    <div class="col-sm-9">
      <!-- required para campos obligatorios -->
      <select class="form-control" name="fk_id_persona" id="fk_id_persona" required>
        <option value="">----Seleccione una Persona-----</option>
        <?php if ($listadoPersonas): ?>
          <?php foreach ($listadoPersonas->result() as $personaTemporal): ?>
            <option value="<?php echo $personaTemporal->id_persona; ?>"><?php echo $personaTemporal->cedula_persona; ?></option>
          <?php endforeach; ?>
        <?php endif; ?>

      </select>
    </div>
  </div>
</div>
</div>
<div class="row">
<div class="col-md-6">
  <div class="form-group row">
    <label class="col-sm-3 col-form-label">Lugar de la Dosis:</label>
    <div class="col-sm-9">
      <input type="text"  value="<?php echo $dosis->lugar_dosis; ?>" name="lugar_dosis" id="lugar_dosis" class="form-control" />
    </div>
  </div>
</div>
<div class="col-md-6">
  <div class="form-group row">
    <label class="col-sm-3 col-form-label">Numero de dosis: </label>
    <div class="col-sm-9">
      <select class="form-control" type="text" style="width:60% "name="numero_dosis" id="numero_dosis">
      <option value="">--Seleccione--</option>
      <option value="1">1</option>
      <option value="2">2</option>
      <option value="3">3</option>
      <option value="4">4</option>
    </select>
    </div>
  </div>
</div>
</div>
<div class="row">
<div class="col-md-6">
  <div class="form-group row">
    <label class="col-sm-3 col-form-label">Vacunador:</label>
    <div class="col-sm-9">
      <input type="text"  value="<?php echo $dosis->vacunador_dosis; ?>" name="vacunador_dosis" id= "vacunador_dosis" class="form-control" />
    </div>
  </div>
</div>
<div class="col-md-6">
  <div class="form-group row">
    <label class="col-sm-3 col-form-label">Fecha: </label>
    <div class="col-sm-9">
      <input type="date" value="<?php echo $dosis->fecha_dosis; ?>" class="form-control" name="fecha_dosis" id= "fecha_dosis" />
    </div>
  </div>
</div>
</div>
<div class="row" align="center">
  <div class="col-md-12">
    <div class="form-group row">
      <button type="submit" class="btn btn-dark btn-lg" align="center"><i class="fa fa-save"></i>&nbsp;ACTUALIZAR</button>
      &nbsp;&nbsp;&nbsp;
        <a href="<?php echo site_url('dosiss/index'); ?>"class="btn btn-danger btn-lg" align="center"><i class="fa fa-times"></i>&nbsp;CANCELAR</a>
    </div>
  </div>
</div>
    <input type="range" name="" value="">
</form>
</div>
</div>
</div>
</form>
<script type="text/javascript">
$("#fk_id_va").val("<?php  echo $dosis->fk_id_va; ?>");
$("#fk_id_persona").val("<?php  echo $dosis->fk_id_persona; ?>");
$("#numero_dosis").val("<?php  echo $dosis->numero_dosis; ?>");

</script>
