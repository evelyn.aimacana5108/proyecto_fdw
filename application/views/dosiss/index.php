<br>
<center>
  <b><i><h2>Listado de Dosis</h2></i></i></b>
</center>
<hr>
<br>
<center>
  <div class=" col-12 d-grid gap-2 col-6 mx-auto text-uppercase">
  <button  class="btn btn-secondary" type="button"><b><a href="<?php echo site_url(); ?>/dosiss/nuevo"><i class="fa fa-plus-circle"></i>Agregar Dosis</a></b></button>
</div>
<hr>
  <br>
  <br>
  <br>
  <br>
</center>
<?php if ($listadoDosiss): ?>
  <table class="table table-bordered table-striped table-hover" id="tbl-dosiss">
    <thead class="table-dark">
    <tr>
      <th class="text-center">ID</th>
      <th class="text-center">LUGAR DE DOSIS</th>
      <th class="text-center">NUMERO DOSIS</th>
      <th class="text-center">VACUNADO POR</th>
      <th class="text-center">FECHA DE DOSIS</th>
      <th class="text-center">TIPO</th>
      <th class="text-center">CEDULA</th>
      <th class="text-center">OPCIONES</th>
    </tr>
    </thead>
    <tbody>
      <?php foreach ($listadoDosiss->result() as $filaTemporal): ?>
        <tr>
          <td class="text-center"><?php echo $filaTemporal->id_dosis;?></td>
          <td class="text-center"><?php echo $filaTemporal->lugar_dosis;?></td>
          <td class="text-center">
            <?php if ($filaTemporal->numero_dosis=="1"): ?>
              <div class="alert alert-success">
                <?php echo $filaTemporal->numero_dosis;?>
              </div>

            <?php else: ?>
              <div class="alert alert-danger">
                <?php echo $filaTemporal->numero_dosis;?>
              </div>

            <?php endif; ?></td>
            <td class="text-center"><?php echo $filaTemporal->vacunador_dosis;?></td>
            <td class="text-center"><?php echo $filaTemporal->fecha_dosis;?></td>
           <th><?php echo $filaTemporal->tipo_va; ?></th>
           <th><?php echo $filaTemporal->cedula_persona; ?></th>


<td class="text-center">
  <a href="<?php echo site_url(); ?>/Dosiss/editar/<?php echo $filaTemporal->id_dosis;?>"class="btn btn-warning"><i class="fa fa-pen"></i></a>
    <a onclick="confirmarEliminacion('<?php echo $filaTemporal->id_dosis; ?>')" href="javascript:void(0)" class="btn btn-danger"><i class="fa fa-trash"></i></a>
</td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
<?php else: ?>
  <div class="alert alert-danger">
    <h3>No se encontraron dosiss resgistrados</h3>
  </div>
<?php endif; ?>

<script type="text/javascript">
    function confirmarEliminacion(id_cli){
          iziToast.question({
              timeout: 20000,
              close: false,
              overlay: true,
              displayMode: 'once',
              id: 'question',
              zindex: 999,
              title: 'CONFIRMACIÓN',
              message: '¿Esta seguro de eliminar la dosis de forma pernante?',
              position: 'center',
              buttons: [
                  ['<button><b>SI</b></button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                      window.location.href=
                      "<?php echo site_url(); ?>/dosiss/procesarEliminacion/"+id_cli;

                  }, true],
                  ['<button>NO</button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                  }],
              ]
          });
    }
</script>
<script type="text/javascript">
$(document).ready(function() {
  $('#tbl-dosiss').DataTable( {
    dom: 'Blfrtip',
    buttons: [
        'copyHtml5',
        'excelHtml5',
        'csvHtml5',
        'pdfHtml5'
    ],
      "order": [[ 3, "desc" ]],
    language: {
      "decimal":        "",
  "emptyTable":     "No hay datos",
  "info":           "Mostrando START a END de TOTAL registros",
  "infoEmpty":      "Mostrando 0 a 0 de 0 registros",
  "infoFiltered":   "(Filtro de MAX total registros)",
  "infoPostFix":    "",
  "thousands":      ",",
  "lengthMenu":     "Mostrar MENU registros",
  "loadingRecords": "Cargando...",
  "processing":     "Procesando...",
  "search":         "Buscar:",
  "zeroRecords":    "No se encontraron coincidencias",
  "paginate": {
      "first":      "Primero",
      "last":       "Ultimo",
      "next":       "Próximo",
      "previous":   "Anterior"
  },
  "aria": {
      "sortAscending":  ": Activar orden de columna ascendente",
      "sortDescending": ": Activar orden de columna desendente"
  }
    }
  } );
} );

</script>
