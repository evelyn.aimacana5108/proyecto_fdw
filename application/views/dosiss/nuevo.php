<br>
<center>
  <ul class="nav nav-tabs">
    <li class="nav-item">
      <a class="nav-link active" aria-current="page" href="<?php echo site_url('dosiss/index'); ?>">LISTADO DE DOSIS</a>
    </li>
  </ul>
</center>
<br>
<form  action="<?php echo site_url(); ?>/dosiss/guardarDosis" method="post" id="frm_nuevo_dosis"  enctype="multipart/form-data">
  <div class="col-12 grid-margin">
  <div class="card">
    <div class="card-body">
      <h4 class="card-title">INGRESAR DATOS DEL CLIENTE</h4>
      <form class="form-sample">
  <p class="card-description"> Ingrese todos los campos vacios </p>
  <br>
<div class="row">
  <div class="col-md-6">
    <div class="form-group row">
      <label class="col-sm-3 col-form-label">Vacuna:</label>
      <div class="col-sm-9">
        <!-- required para campos obligatorios -->
        <select class="form-control" name="fk_id_va" id="fk_id_va" required>
          <option value="">----Seleccioneun vacuna-----</option>
          <?php if ($listadoVacunas): ?>
            <?php foreach ($listadoVacunas->result() as $vacunaTemporal): ?>
              <option value="<?php echo $vacunaTemporal->id_va; ?>"><?php echo $vacunaTemporal->tipo_va; ?></option>
            <?php endforeach; ?>
          <?php endif; ?>

        </select>
      </div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group row">
      <label class="col-sm-3 col-form-label">Persona </label>
      <div class="col-sm-9">
        <!-- required para campos obligatorios -->
        <select class="form-control" name="fk_id_persona" id="fk_id_persona" required>
          <option value="">----Seleccione una Persona-----</option>
          <?php if ($listadoPersonas): ?>
            <?php foreach ($listadoPersonas->result() as $personaTemporal): ?>
              <option value="<?php echo $personaTemporal->id_persona; ?>"><?php echo $personaTemporal->cedula_persona; ?></option>
            <?php endforeach; ?>
          <?php endif; ?>

        </select>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-6">
    <div class="form-group row">
      <label class="col-sm-3 col-form-label">Lugar de la Dosis:</label>
      <div class="col-sm-9">
        <input type="text"  name="lugar_dosis" id="lugar_dosis" class="form-control" />
      </div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group row">
      <label class="col-sm-3 col-form-label">Numero de dosis: </label>
      <div class="col-sm-9">
        <select class="form-control" type="text" style="width:60% "name="numero_dosis" id="numero_dosis">
        <option value="">--Seleccione--</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
      </select>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-6">
    <div class="form-group row">
      <label class="col-sm-3 col-form-label">Vacunador:</label>
      <div class="col-sm-9">
        <input type="text"  name="vacunador_dosis" id= "vacunador_dosis" class="form-control" />
      </div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group row">
      <label class="col-sm-3 col-form-label">Fecha: </label>
      <div class="col-sm-9">
        <input type="date" class="form-control" name="fecha_dosis" id= "fecha_dosis" />
      </div>
    </div>
  </div>
</div>
    <div class="row">
      <div class="col-md-12 mt-4 mb-4 text-center">
        <button type="submit" class="btn btn-dark btn-lg" align="center"><i class="fa fa-save"></i>Guardar</b></button>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <a href="<?php echo site_url('dosiss/index'); ?>"class="btn btn-danger btn-lg" align="center"><i class="fa fa-times"></i>&nbsp;CANCELAR</a>
        </a>
      </div>
  </div>
  <input type="range" name="" value="">
</form>
</div>
</div>
</div>
</form>
<script type="text/javascript">
  $('#frm_nuevo_dosis').validate({
      rules:{
        fk_id_va:{
          required:true,

        },
        fk_id_persona:{
          required:true,

        },
        lugar_dosis:{
          letras:true,
          required:true,
        },
        numero_dosis:{
          required:true,

        },
        vacunador_dosis:{
          letras:true,
          required:true,

        },
        fecha_dosis:{
            required:true,
          date:true,
        }
      },
      messages:{
        fk_id_va:{
          required:"Por favor ingrese la vacuna",

        },
        fk_id_persona:{
          required:"Por favor ingrese la persona",

        },
        lugar_dosis:{
          letras:"Nombre incorrecto",
          required:"Por favo ingrese el nombre de la dosis",
        },
        numero_dosis:{
          required:"Por favo ingrese el numero de dosis"
        },
        vacunador_dosis:{
          letras:"Apellido incorrecto",
          required:"Por favo ingrese el nombre de la persona quien lo vacuno"

        },
        fecha_dosis:{
          required:"ingrese una fecha",
          date:"Fecha incorrecto"

        }
      }

  });
</script>
</script>
<script type="text/javascript">
  $("#foto_cli").fileinput({
    allowedFileExtension:["jpeg","jpg","png"],
    dropZoneEnabled:true,
    language:"es"
  });
</script>
