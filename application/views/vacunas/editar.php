<form class=""
action="<?php echo site_url('vacunas/actualizarVacunaAjax'); ?>"
method="post"
id="frm_actualizar_vacuna">

    <input type="hidden" name="id_va" id="id_va"
    value="<?php echo $vacuna->id_va; ?>">

    <label for="">NOMBRE:</label><br>
    <input type="text" name="tipo_va"
    value="<?php echo $vacuna->tipo_va; ?>"
    id="tipo_va" class="form-control"> <br>

    <label for="">FOTO:</label><br>
    <input type="file" class="form-control" value="<?php echo $vacuna->foto_va; ?>" name="foto_va" id='foto_va' class="form-control input-sm " accept="image/*"  >
> <br>


    <br>
    <button type="button" onclick="actualizar();" name="button"
    class="btn btn-success">
      <i class="fa fa-pen"></i> Actualizar
    </button>
</form>


<script type="text/javascript">

function actualizar(){

    $.ajax({
      url:$("#frm_actualizar_vacuna").prop("action"),
      data:$("#frm_actualizar_vacuna").serialize(),
      type:"post",
      success:function(data){
        cargarListadoVacunas();
        $("#modalEditarVacuna").modal("hide");
        $('body').removeClass('modal-open');//eliminamos la clase del body para poder hacer scroll
        $('.modal-backdrop').remove();//eliminamos el backdrop del modal
        var objetoJson=JSON.parse(data);
        if(objetoJson.respuesta=="ok" || objetoJson.respuesta=="OK"){
          iziToast.success({
               title: 'CONFIRMACIÓN',
               message: 'Actualización Exitosa',
               position: 'topRight',
             });
        }else{
          iziToast.error({
               title: 'ERROR',
               message: 'Error al procesar',
               position: 'topRight',
             });
        }

      }
    });
}

</script>

<script type="text/javascript">

  $('#foto_va').fileinput({
    allowedFileExtensions:['jpeg','jpg','png'],
    dropZoneEnable:true,
    language:'es'

  });



</script>
