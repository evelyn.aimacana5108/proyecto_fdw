
<form class=""
action="<?php echo site_url('usuarios/actualizarUsuarioAjax'); ?>"
method="post"
id="frm_actualizar_usuario">

    <input type="hidden" name="id_usuario" id="id_usuario"
    value="<?php echo $usuario->id_usuario; ?>">

    <label for="">APELLIDO:</label><br>
    <input type="text" name="apellido_usuario"
    value="<?php echo $usuario->apellido_usuario; ?>"
    id="apellido_usuario_editar" class="form-control" required> <br>

    <label for="">NOMBRE:</label><br>
    <input type="text" name="nombre_usuario"
    value="<?php echo $usuario->nombre_usuario; ?>"
    id="nombre_usuario_editar" class="form-control"> <br>

    <label for="">EMAIL:</label><br>
    <input type="text" name="email_usuario"
    value="<?php echo $usuario->email_usuario; ?>"
    id="email_usuario_editar" class="form-control"> <br>
    <label for="">CONTRASEÑA:</label>
    <br>
    <input type="password" name="password_usuario"
    value="<?php echo $usuario->password_usuario; ?>"
    id="password_usuario" class="form-control"> <br>
    <label for="">PERFIL:</label>
    <br>
    <select class="form-control" name="perfil_usuario"
    id="perfil_usuario_editar">
     <option value="">Seleccione una opción</option>
     <option value="ADMINISTRADOR">ADMINISTRADOR</option>
     <option value="VENDEDOR">VENDEDOR</option>
    </select>
    <script type="text/javascript">
        $("#perfil_usuario_editar").val("<?php echo $usuario->perfil_usuario; ?>");
    </script>

    <br>
    <button type="button" onclick="actualizar();" name="button"
    class="btn btn-success">
      <i class="fa fa-pen"></i> Actualizar
    </button>
</form>


<script type="text/javascript">

function actualizar(){

    $.ajax({
      url:$("#frm_actualizar_usuario").prop("action"),
      data:$("#frm_actualizar_usuario").serialize(),
      type:"post",
      success:function(data){
        cargarListadoUsuarios();
        $("#modalEditarUsuario").modal("hide");
        $('body').removeClass('modal-open');//eliminamos la clase del body para poder hacer scroll
        $('.modal-backdrop').remove();//eliminamos el backdrop del modal
        var objetoJson=JSON.parse(data);
        if(objetoJson.respuesta=="ok" || objetoJson.respuesta=="OK"){
          iziToast.success({
               title: 'CONFIRMACIÓN',
               message: 'Actualización Exitosa',
               position: 'topRight',
             });
        }else{
          iziToast.error({
               title: 'ERROR',
               message: 'Error al procesar',
               position: 'topRight',
             });
        }

      }
    });
}

</script>
