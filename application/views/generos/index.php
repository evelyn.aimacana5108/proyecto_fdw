<br>
<center>
  <b><i><h2>LISTADO</h2></i></i></b>
</center>
<hr>
<br>
<center>
  <div class=" col-12 d-grid gap-2 col-6 mx-auto text-uppercase">
  <button  class="btn btn-secondary" type="button"><b><a href="<?php echo site_url(); ?>/generos/nuevo" ><i class="fa fa-plus-circle"></i>Agregar Nuevo</a></b></button>
</div>
<hr>
  <br>
  <br>
  <br>
  <br>
</center>
<?php if ($listadoGeneros): ?>
  <table class="table table-bordered table-striped table-hover" id="tbl-generos">
    <thead class="table-dark">
    <tr>
      <th class="text-center">ID</th>
      <th class="text-center">GENERO</th>
      <th class="text-center">OPCIONES</th>
    </tr>
    </thead>
    <tbody>
      <?php foreach ($listadoGeneros->result() as $filaTemporal): ?>
        <tr>
          <td class="text-center"><?php echo $filaTemporal->id_genero;?></td>
          <td class="text-center"><?php echo $filaTemporal->nombre_genero;?></td>
<td class="text-center">
  <a href="<?php echo site_url(); ?>/Generos/editar/<?php echo $filaTemporal->id_genero;?>"class="btn btn-warning"><i class="fa fa-pen"></i></a>
    <a onclick="confirmarEliminacion('<?php echo $filaTemporal->id_genero; ?>')" href="javascript:void(0)" class="btn btn-danger"><i class="fa fa-trash"></i></a>
</td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
<?php else: ?>
  <div class="alert alert-danger">
    <h3>No se encontraron generos resgistrados</h3>
  </div>
<?php endif; ?>

<script type="text/javascript">
    function confirmarEliminacion(id_genero){
          iziToast.question({
              timeout: 20000,
              close: false,
              overlay: true,
              displayMode: 'once',
              id: 'question',
              zindex: 999,
              title: 'CONFIRMACIÓN',
              message: '¿Esta seguro de eliminar el genero de forma pernante?',
              position: 'center',
              buttons: [
                  ['<button><b>SI</b></button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                      window.location.href=
                      "<?php echo site_url(); ?>/generos/procesarEliminacion/"+id_genero;

                  }, true],
                  ['<button>NO</button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                  }],
              ]
          });
    }
</script>
<script type="text/javascript">
$(document).ready( function () {
  $('#tbl-lineas').DataTable({
    dom: 'Blfrtip',
    buttons: [
        'copyHtml5',
        'excelHtml5',
        'csvHtml5',
        'pdfHtml5'
    ],
      "order": [[ 3, "desc" ]],
    language: {
      "decimal":        "",
  "emptyTable":     "No hay datos",
  "info":           "Mostrando _START_ a _END_ de _TOTAL_ registros",
  "infoEmpty":      "Mostrando 0 a 0 de 0 registros",
  "infoFiltered":   "(Filtro de _MAX_ total registros)",
  "infoPostFix":    "",
  "thousands":      ",",
  "lengthMenu":     "Mostrar _MENU_ registros",
  "loadingRecords": "Cargando...",
  "processing":     "Procesando...",
  "search":         "Buscar:",
  "zeroRecords":    "No se encontraron coincidencias",
  "paginate": {
      "first":      "Primero",
      "last":       "Ultimo",
      "next":       "Próximo",
      "previous":   "Anterior"
  },
  "aria": {
      "sortAscending":  ": Activar orden de columna ascendente",
      "sortDescending": ": Activar orden de columna desendente"
  }
    }
  } );
} );

</script>
<script type="text/javascript">
$(document).ready( function () {
  $('#tbl-lineas').DataTable({
    dom: 'Blfrtip',
    buttons: [
        'copyHtml5',
        'excelHtml5',
        'csvHtml5',
        'pdfHtml5'
    ],
  });
} );
</script>
