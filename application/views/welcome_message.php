<div id="carouselExampleInterval" class="carousel slide" data-bs-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active" data-bs-interval="100">
      <img src="https://www.cuidate-murcia.com/wp-content/uploads/2021/01/Rucuperar-el-Olfato-y-gusto-Cuidate-Murcia-13-1024x576.jpg" class="d-block w-100" alt="...">
    </div>
    <div class="carousel-item" data-bs-interval="20">
      <img src="https://cieg.unam.mx/covid-genero/images/notas/foto-nota-4786.jpg" class="d-block w-100" alt="...">
    </div>
    <div class="carousel-item">
      <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTb5NGDUDUdoRmHgV5xN21MxHCTjgcnJ5FXuQ&usqp=CAU" class="d-block w-100" alt="...">
    </div>
  </div>
  <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleInterval" data-bs-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Vistas</span>
  </button>
  <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleInterval" data-bs-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Siguiente</span>
  </button>
</div>
<!-- Team Start -->
<div class="container-xxl py-5">
		<div class="container">
				<div class="text-center mx-auto mb-5 wow fadeInUp" data-wow-delay="0.1s" style="max-width: 600px;">
						<p class="d-inline-block border rounded-pill py-1 px-4">Doctors</p>
						<h1><p> Es una estrategia para
separar a aquellas
personas que han sido
diagnosticadas positivas
para COVID -19 o tienen
síntomas respiratorios, de
aquellas que están
saludables.</h1>
				</div>
				<div class="row g-4">
						<div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.1s">
								<div class="team-item position-relative rounded overflow-hidden">
										<div class="overflow-hidden">
												<img class="img-fluid" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQOB5c7mTuGUzcuyjmuwqMQYEb9u6SQQPafvw&usqp=CAU" alt="">
										</div>
										<div class="team-text bg-light text-center p-4">
												<h5>Doctor Name</h5>
												<p class="text-primary">Department</p>
												<div class="team-social text-center">
														<a class="btn btn-square" href=""><i class="fab fa-facebook-f"></i></a>
														<a class="btn btn-square" href=""><i class="fab fa-twitter"></i></a>
														<a class="btn btn-square" href=""><i class="fab fa-instagram"></i></a>
												</div>
										</div>
								</div>
						</div>
						<div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.3s">
								<div class="team-item position-relative rounded overflow-hidden">
										<div class="overflow-hidden">
												<img class="img-fluid" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRxqtwTByABXSzBXanvgOwiQw1ORksIPAT842AuoHVV17zLyooRN2qA4wNBD17ROKF_g5g&usqp=CAU" alt="">
										</div>
										<div class="team-text bg-light text-center p-4">
												<h5>Doctor Name</h5>
												<p class="text-primary">Department</p>
												<div class="team-social text-center">
														<a class="btn btn-square" href=""><i class="fab fa-facebook-f"></i></a>
														<a class="btn btn-square" href=""><i class="fab fa-twitter"></i></a>
														<a class="btn btn-square" href=""><i class="fab fa-instagram"></i></a>
												</div>
										</div>
								</div>
						</div>
						<div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.5s">
								<div class="team-item position-relative rounded overflow-hidden">
										<div class="overflow-hidden">
												<img class="img-fluid" src="https://www.redaccionmedica.com/images/destacados/fresenius-helios-clinicas-fertilidad-eugin-430-millones-2256_620x368.jpg" alt="">
										</div>
										<div class="team-text bg-light text-center p-4">
												<h5>Doctor Name</h5>
												<p class="text-primary">Department</p>
												<div class="team-social text-center">
														<a class="btn btn-square" href=""><i class="fab fa-facebook-f"></i></a>
														<a class="btn btn-square" href=""><i class="fab fa-twitter"></i></a>
														<a class="btn btn-square" href=""><i class="fab fa-instagram"></i></a>
												</div>
										</div>
								</div>
						</div>
						<div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.7s">
								<div class="team-item position-relative rounded overflow-hidden">
										<div class="overflow-hidden">
												<img class="img-fluid" src="https://www.creditosenusa.com/wp-content/uploads/como-encontrar-clinicas-cerca-de-mi.jpg" alt="">
										</div>
										<div class="team-text bg-light text-center p-4">
												<h5>Doctor Name</h5>
												<p class="text-primary">Department</p>
												<div class="team-social text-center">
														<a class="btn btn-square" href=""><i class="fab fa-facebook-f"></i></a>
														<a class="btn btn-square" href=""><i class="fab fa-twitter"></i></a>
														<a class="btn btn-square" href=""><i class="fab fa-instagram"></i></a>
												</div>
										</div>
								</div>
						</div>
				</div>
		</div>
</div>
<!-- Team End -->
