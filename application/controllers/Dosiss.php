<?php
    class Dosiss extends CI_Controller{
        public function __construct(){
          parent::__construct();
          $this->load->model('dosis');
          $this->load->model("vacuna");
          $this->load->model("persona");
        }

        public function index(){
          $data["listadoDosiss"]=$this->dosis->consultarTodos();
          $this->load->view('header');
          $this->load->view('dosiss/index',$data);
          $this->load->view('footer');
        }

        public function nuevo(){
          $data["listadoVacunas"]=$this->vacuna->obtenerTodos();
          $data["listadoPersonas"]=$this->persona->consultarTodos();
          $this->load->view('header');
          $this->load->view('dosiss/nuevo',$data);
          $this->load->view('footer');
        }
        public function editar($id_dosis){
          $data["listadoVacunas"]=$this->vacuna->consultarTodos();
          $data["listadoPersonas"]=$this->persona->consultarTodos();
          $data["dosis"]=$this->dosis->consultarPorId($id_dosis);
          $this->load->view('header');
          $this->load->view('dosiss/editar',$data);
          $this->load->view('footer');
        }
        public function procesarActualizacion(){
          $id_dosis=$this->input->post("id_dosis");
          $datosDosisEditado=array(
            "lugar_dosis"=>$this->input->post("lugar_dosis"),
            "numero_dosis"=>$this->input->post("numero_dosis"),
            "vacunador_dosis"=>$this->input->post("vacunador_dosis"),
            "fecha_dosis"=>$this->input->post("fecha_dosis"),
            "fk_id_va"=>$this->input->post("fk_id_va"),
            "fk_id_persona"=>$this->input->post("fk_id_persona"),
          );
          if ($this->dosis->actualizar($id_dosis,$datosDosisEditado)) {
            //echo "INSERCION EXITOSA";
            redirect("dosiss/index");
          } else {
            echo "ERROR AL ACTUALIZAR";
          }
        }

        //registro de usuarios
        public function guardarDosis(){
          $datosNuevoDosis=array(
            "lugar_dosis"=>$this->input->post("lugar_dosis"),
            "numero_dosis"=>$this->input->post("numero_dosis"),
            "vacunador_dosis"=>$this->input->post("vacunador_dosis"),
            "fecha_dosis"=>$this->input->post("fecha_dosis"),
            "fk_id_va"=>$this->input->post("fk_id_va"),
            "fk_id_persona"=>$this->input->post("fk_id_persona"),
          );

          if ($this->dosis->insertar($datosNuevoDosis)) {
              $this->session->set_flashdata("confirmacion","DOSIS INSERTADO EXITOSAMENTE.");
          } else {
            $this->session->set_flashdata("error","ERROR AL PROCESAR, INTENTE NUEVAMENTE.");
          }
          redirect("dosiss/index");

        }
        function procesarEliminacion($id_dosis){
            if($this->dosis->eliminar($id_dosis)){
              redirect("dosiss/index");
            }else{
              echo "Error al eliminar";
            }
    }
    }//cierre de la clase
 ?>
