<?php
    class Generos extends CI_Controller{
        public function __construct(){
          parent::__construct();
          $this->load->model("genero");
        }

        public function index(){
          $data["listadoGeneros"]=$this->genero->consultarTodos();
          $this->load->view('header');
          $this->load->view('generos/index',$data);
          $this->load->view('footer');
        }

        public function nuevo(){
          $this->load->view('header');
          $this->load->view('generos/nuevo');
          $this->load->view('footer');
        }
        public function editar($id_genero){
          $data["genero"]=$this->genero->consultarPorId($id_genero);
          $this->load->view('header');
          $this->load->view('generos/editar',$data);
          $this->load->view('footer');
        }
        public function procesarActualizacion(){
          $id_genero=$this->input->post("id_genero");
          $datosGeneroEditado=array(
            "nombre_genero"=>$this->input->post("nombre_genero"),
          );
          if ($this->genero->actualizar($id_genero,$datosGeneroEditado)) {
            //echo "INSERCION EXITOSA";
            redirect("generos/index");
          } else {
            echo "ERROR AL ACTUALIZAR";
          }
        }

        //registro de usuarios
        public function guardarGenero(){
          $datosNuevoGenero=array(
            "nombre_genero"=>$this->input->post("nombre_genero"),
          );

          if ($this->genero->insertar($datosNuevoGenero)) {
              $this->session->set_flashdata("confirmacion","GENERO INSERTADO EXITOSAMENTE.");
          } else {
            $this->session->set_flashdata("error","ERROR AL PROCESAR, INTENTE NUEVAMENTE.");
          }
          redirect("generos/index");

        }
        function procesarEliminacion($id_genero){
            // code...
            if($this->genero->eliminar($id_genero)){
              redirect("generos/index");
            }else{
              echo "Error al eliminar";
            }

    }
    }//cierre de la clase
 ?>
