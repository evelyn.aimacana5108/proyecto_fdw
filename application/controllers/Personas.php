<?php
    class Personas extends CI_Controller{
        public function __construct(){
          parent::__construct();
          $this->load->model('persona');
          $this->load->model("genero");
        }

        public function index(){
          $data["listadoPersonas"]=$this->persona->consultarTodos();
          $this->load->view('header');
          $this->load->view('personas/index',$data);
          $this->load->view('footer');
        }

        public function nuevo(){
          $data["listadoGeneros"]=$this->genero->consultarTodos();
          $this->load->view('header');
          $this->load->view('personas/nuevo',$data);
          $this->load->view('footer');
        }
        public function editar($id_persona){
          $data["listadoGeneros"]=$this->genero->consultarTodos();
          $data["persona"]=$this->persona->consultarPorId($id_persona);
          $this->load->view('header');
          $this->load->view('personas/editar',$data);
          $this->load->view('footer');
        }
        public function procesarActualizacion(){
          $id_persona=$this->input->post("id_persona");
          $datosPersonaEditado=array(
            "cedula_persona"=>$this->input->post("cedula_persona"),
            "nombre_persona"=>$this->input->post("nombre_persona"),
            "fk_id_genero"=>$this->input->post("fk_id_genero"),
          );
          $this->load->library("upload");
          $new_name = "foto_persona_" . time() . "_" . rand(1, 5000);
          $config["file_name"]=$nombreTemporal;
          $config["upload_path"]=APPPATH.'../uploads/personas/';
          $config['allowed_types']        = 'jpeg|jpg|png';
          $config['max_size']             = 4*1024;
          $this->upload->initialize($config);

          if ($this->upload->do_upload("foto_persona")) {
            $dataSubida = $this->upload->data();
            $datosPersonaEditado["foto_persona"] = $dataSubida['file_name'];
          }


          if ($this->persona->actualizar($id_persona,$datosPersonaEditado)) {
            //echo "INSERCION EXITOSA";
            redirect("personas/index");
          } else {
            echo "ERROR AL ACTUALIZAR";
          }
        }

        //registro de usuarios
        public function guardarPersona(){
          $datosNuevoPersona=array(
            "cedula_persona"=>$this->input->post("cedula_persona"),
            "nombre_persona"=>$this->input->post("nombre_persona"),
            "fk_id_genero"=>$this->input->post("fk_id_genero"),
          );

          $this->load->library("upload");
          $nombreTemporal="foto_persona_".time()."_".rand(1,5000);
          $config["file_name"]=$nombreTemporal;
          $config["upload_path"]=APPPATH.'../uploads/personas/';
          $config["allowed_types"]="jpeg|jpg|png";
          $config["max_size"]=2*1024;//2mb
          $this->upload->initialize($config);

          if ($this->upload->do_upload("foto_persona")) {
            $dataSubida=$this->upload->data();
            $datosNuevoPersona["foto_persona"]=$dataSubida["file_name"];
          }

          if ($this->persona->insertar($datosNuevoPersona)) {
              $this->session->set_flashdata("confirmacion","PERSONA INSERTADO EXITOSAMENTE.");
          } else {
            $this->session->set_flashdata("error","ERROR AL PROCESAR, INTENTE NUEVAMENTE.");
          }
          redirect("personas/index");

        }
        function procesarEliminacion($id_persona){
            // code...
            if($this->persona->eliminar($id_persona)){
              redirect("personas/index");
            }else{
              echo "Error al eliminar";
            }

    }
    }//cierre de la clase
 ?>
