<?php
class Usuarios extends CI_Controller{
    public function __construct(){
        parent::__construct();
        $this->load->model("usuario");
        //validando si alguien esta conectado ESTO ES LO PRIMORDIAL
        if ($this->session->userdata("c0nectadoUTC")) {
          // si esta conectado
          if ($this->session->userdata("c0nectadoUTC")->perfil_usuario=="ADMINISTRADOR")
          {
            // SI ES ADMINISTRADOR
          } else {
            redirect("/");
          }
        } else {
          redirect("seguridades/formularioLogin");
        }
    }

  public function index(){
    $this->load->view("header");
    $this->load->view("usuarios/index");
    $this->load->view("footer");
  }
  //funcion para frenderizar la vista listado.php
  public function listado(){
    $data["listadoUsuarios"]=$this->usuario->obtenerTodos();
    $this->load->view("usuarios/listado",$data);

  }
  ///Inserciion Asincrona
  public function insertarUsuario(){
    $data=array(
    "apellido_usuario"=>$this->input->post("apellido_usuario"),
    "nombre_usuario"=>$this->input->post("nombre_usuario"),
    "email_usuario"=>$this->input->post("email_usuario"),
    "password_usuario"=>$this->input->post("password_usuario"),
    "perfil_usuario"=>$this->input->post("perfil_usuario")
    );
    if($this->usuario->insertarUsuario($data)){
      echo json_encode(array("respuesta"=>"ok"));
    }else{
      echo json_encode(array("respuesta"=>"error"));

    }
  }
  public function eliminarUsuario(){
    $id_usuario=$this->input->post("id_usuario");
    if ($this->usuario->eliminar($id_usuario)) {
      echo json_encode(array("respuesta"=>"ok"));
    } else{
      echo json_encode(array("respuesta"=>"error"));
    }
  }

  public function editar($ide_usuario){
      $data["usuario"]=$this->usuario->obtenerPorId($ide_usuario);
      $this->load->view("usuarios/editar",$data);
      }

      public function actualizarUsuarioAjax(){
              $id_usu=$this->input->post("id_usuario");
              $data=array(
                  "apellido_usuario"=>$this->input->post("apellido_usuario"),
                  "nombre_usuario"=>$this->input->post("nombre_usuario"),
                  "email_usuario"=>$this->input->post("email_usuario"),
                  "perfil_usuario"=>$this->input->post("perfil_usuario")
              );
              if($this->usuario->actualizar($data,$id_usuario)){
                  echo json_encode(array("respuesta"=>"ok"));
              }else{
                  echo json_encode(array("respuesta"=>"error"));
              }
          }


  }//cierre de la clase
