<?php
    class Seguridades extends CI_Controller{
        public function __construct(){
          parent::__construct();
          $this->load->model('usuario');
        }

        public function formularioLogin(){
            $this->load->view("seguridades/formularioLogin");
        }
        public function recuperar(){
          $this->load->view("seguridades/recuperar");
        }
        public function registrar(){
          $this->load->view("seguridades/registrar");
        }

     public function validarAcceso(){
     $email_usuario=$this->input->post("email_usuario");
     $password_usuario=$this->input->post("password_usuario");
     $usuario= $this->usuario->buscarUsuarioPorEmailPassword($email_usuario, $password_usuario);
     if($usuario){

            $this->session->set_userdata("c0nectadoUTC", $usuario);
            $this->session->set_flashdata("bienvenida", "Usuario Conectado");
            redirect("usuarios/index");
     }
     else{//cuando el email y contraseña son incorrectos
        $this->session->set_flashdata("error", "Email o contraseña incorrecta");
        redirect("seguridades/formularioLogin");

     }
     }
     public function cerrarSesion(){
     $this->session->set_flashdata("salir","Salir, ");
   $this->session->sess_destroy();//Matando la sesiones

   redirect("seguridades/formularioLogin");
 }

 public function insertarUsuario(){
   $data=array(
   "apellido_usuario"=>$this->input->post("apellido_usuario"),
   "nombre_usuario"=>$this->input->post("nombre_usuario"),
   "email_usuario"=>$this->input->post("email_usuario"),
   "password_usuario"=>$this->input->post("password_usuario"),
   "perfil_usuario"=>$this->input->post("perfil_usuario")
   );
   if($this->usuario->insertarUsuario($data)){
     echo json_encode(array("respuesta"=>"ok"));
   }else{
     echo json_encode(array("respuesta"=>"error"));

   }
 }


 public function pruebaEmail(){
      enviarEmail("veronica.taipe9221@utc.edu.ec","PRUEBA","<h1>HOLA</h1><i>MUNDO</i>");
      }

      public function recuperarPassword(){
          $email=$this->input->post("email");
          $usuario=$this->usuario->obtenerPorEmail($email);
          if($usuario){
            $password_aleatorio=rand(111111,999999);
            $asunto="RECUPERAR PASSWORD";
            $contenido="Su contraseña temporal es: <b> $password_aleatorio</b>";
            enviarEmail($email,$asunto,$contenido);
            $data=array(
              "password_usuario"=>$password_aleatorio
            );
            $this->usuario->actualizar($data,$usuario->id_usuario);

            $this->session->set_flashdata("confirmacion","Hemos enviado una clave temporal a su direccion de email");
          } else{

              $this->session->set_flashdata("error","El email ingresado no existe");

          }

          redirect("seguridades/formularioLogin");

        }



}//cier
